function addTokens(input, tokens){
    let newinput;
    if (typeof (input) === 'string') {
    if (input.length >6) {
    tokens.forEach((e) => {
        if (e instanceof Object)
        {
            if(!(e.hasOwnProperty('tokenName'))){
              throw new Error('Invalid array format');
            }
       Object.values(e).forEach((x)=>{
           if(typeof x!=='string'){
            throw new Error('Invalid array format');
           }
       })
       }
      
       });
    if (input.indexOf('...') == -1) {
        return input;
    }
    else {
        tokens.map((e) => {
            newinput = input.replace('...', '${' + e.tokenName + "}")
        });
        return newinput;
    }
}
else{
    throw new Error('Input should have at least 6 characters');
}
    }
    else{
        throw new Error('Invalid input');
    }
}  

const app = {
    addTokens: addTokens
}

module.exports = app;

//input should be a string. If other type is passed throw an Error with the message Input should be a string; (0.5 pts)
//input should be at least 6 characters long. If input length is less than 6 throw an Error with the message Input should have at least 6 characters; (0.5 pts)
//tokens is an array with elements with the following format: {tokenName: string}. If this format is not respected throw an Error with the following message Invalid array format; (0.5 pts)
//If input don't contain any ... return the initial value of input; (0.5 pts)
//If input contains ..., replace them with the specific values and return the new input; (0.5 pts)